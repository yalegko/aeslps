build_block : source/AESblock.l source/libl0.l
	@ cp source/AESblock.l bin
	@ cp source/libl0.l bin
	@ make -C bin block
	@ rm bin/AESblock.l
	@ rm bin/libl0.l
	
block : build_block txt key
	@ >etxt
	@ >dtxt
	@ echo  -e '\nЦелевые файлы dtxt и etxt очищены'
	@ ./bin/AESblock
	@ echo 'Шифрование выполнено. Результат шифрования etxt, расшифрования - dtxt'

build_crypt : source/AEScrypt.l source/libl0.l
	@ cp source/AEScrypt.l bin
	@ cp source/libl0.l bin
	@ make -C bin crypt
	@ rm bin/AEScrypt.l
	@ rm bin/libl0.l
	
crypt : build_crypt txt key
	@ >etxt
	@ echo -e  '\nЦелевой файл etxt очищен'
	./bin/AEScrypt
	@ echo 'Шифрование выполнено. Результат шифрования etxt'

build_decrypt : source/AESdecrypt.l source/libl0.l
	@ cp source/AESdecrypt.l bin
	@ cp source/libl0.l bin
	@ make -C bin decrypt
	@ rm bin/AESdecrypt.l
	@ rm bin/libl0.l

decrypt : build_decrypt txt key
	@ >dtxt
	@ echo -e '\nЦелевой файл dtxt очищен'
	./bin/AESdecrypt
	@ echo 'Расшифрование выполнено. Результат расшифрования dtxt'
