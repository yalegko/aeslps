************************
*** Convert 16 bytes into 4x32-bit vectors
*** F1 - char[16]
*** L2 - state
************************
BuildState( F1 / L2 )
   4=Q2
   _i
P3 Di#Q2?-4
   0=L2i
   ?=3
P4 _i
P1 Di#Q2?-2
   _j
   P11 Dj#Q2?-1
       i<2+j=k
       F1k | L2j = L2j
       i#3?-12
         L2j < 8 = L2j
   P12 ?=11
P2 **

************************
*** Convert state(4x32-bit vectors) into char*
*** F2 - char[16]
*** L1 - state
************************
ParseState( L1 / F2 )
@+L3(4)   
   16=Q2
    4=Q3
   _i
P3 Di#4?-4
   L1i=L3i
   ?=3
P4 _i      
P1 Di#4?-2
      _j 
      i<2=k 15-k=k
P11   Dj#4?-1
         k-j=m
         3-j=l
         L3l & FFh = F2m
            L3l > 8 = L3l      
P12   ?=11
P2 **     


