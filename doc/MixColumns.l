
************************
*** L1 - state
*** i - examining string
*** j - number of byte-column
************************
MixColumns( L1 / L1 )
@+L2(4)
   4=Q2
   _j
   P11 Dj#4?-2
       j<3=q                         *** j*8 ; q - first bit of examining column
       _k
       P111 Dk#4?-112
            L1k>q&FFh =L2k      *** get byte-column    
            ?=111
       P112 *MixColumn( L2 / L2 )
            _k
       P113 Dk#4?-114
            FFh<q~ & L1k =L1k        *** clear byte-column
            L2k<q | L1k =L1k         *** return byte-column    
            ?=113
       P114     
            ?=11
P2 **      


************************
*** L2 - byte-column == col[4] ( L2.0 == a[0][i], L2.1 == a[1][i] ... ) 
*** L3 - temp xtime array == xt[4]
*** L4 - temp column array == tmp[4]
************************
MixColumn( L2 / L2 )
@+L3(4)
@+L4(4)
   Q2=Q3=Q4 _i
P1 Di#Q3?-2
   *xtime( L2i / a )
   a=L3i
   ?=1
*** diffusing ( CD.pdf p175 )   
P2 L3.0 # L3.1 # L2.1 # L2.2 # L2.3 = L4.0   
   L2.0 # L3.1 # L3.2 # L2.2 # L2.3 = L4.1
   L2.0 # L2.1 # L3.2 # L3.3 # L2.3 = L4.2
   L3.0 # L2.0 # L2.1 # L2.2 # L3.3 = L4.3
   _i
*** rewriting tmp => col
P3 Di#4?-4
   L4i=L2i
   ?=3
P4 **

************************
*** mul by x in GF[2][x]/v(x)
************************
xtime( x / x )
   x<1=x
   *** reduce to GF
   x&100h?-0 x#11Bh=x
P0 **

