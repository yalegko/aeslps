************************
*** L1 - state
*** i - examining string
*** j - number of byte-column
************************
InvMixColumns( L1 / L1 )
@+L2(4)
   4=Q2
   _j
   P11 Dj#4?-2
       j<3=q                         *** j div 8 ; q - first bit of examining column
       _k
       P111 Dk#4?-112
            L1k>q&FFh =L2k           *** get byte-column    
            ?=111
       P112 *InvMixColumn( L2 / L2 ) 
            _k
       P113 Dk#4?-114
            FFh<q~ & L1k =L1k        *** clear byte-column
            L2k<q | L1k =L1k         *** return byte-column    
            ?=113
       P114     
            ?=11
P2 **      


************************
*** L2 - byte-column == col[4] ( L2.0 == a[0][i], L2.1 == a[1][i] ... ) 
*** L3 - temp gf_mul array
*** L4 - temp column array == tmp[4]
************************
InvMixColumn( L2 / L2 )
@+L3(4)
@+L4(4)
   4=Q3=Q4 _i
P1 Di#4?-2
   i=q
      *gf_mul( L2q, 14 / a )
      a=L3.0
   q+1&3=q
      *gf_mul( L2q, 11 / a )
      a=L3.1
   q+1&3=q
      *gf_mul( L2q, 13 / a )
      a=L3.2
   q+1&3=q
      *gf_mul( L2q, 9 / a )
      a=L3.3
   L3.0 # L3.1 # L3.2 # L3.3 = L4i
?=1   

P2   _i
*** rewriting tmp => col
P3 Di#4?-4
   L4i=L2i
   ?=3
P4 **


************************
*** multiplicate a by b in GF[2][x]/v(x)
************************
gf_mul( a, b / c )
   Oc
P1 a?-2
      *** if bit of a is set add b
      a&1?-11 c#b=c
      *** multiply b by x
P11   b<1=b
      *** reduce mod 0x11B == v(x)
      b&100h?-12 b#11Bh=b
P12   *** get next bit of a
      a>1=a
   ?=1
P2 **

