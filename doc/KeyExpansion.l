************************
*** L1 - key
*** L2 - array of round keys
*** L3 - Vi-constants ( s116 )
*** L4 - SBox
************************
KeyExpansion( L1, L4 / L2 )
@+L3(12)
*DefineVi( L3 / L3 )

   44=Q2
   _i
*** 1-4 without changes
P1 Di#4?-12
   L1i=L2i
   ?=1
P12
*** extension; Симметричная криптография  Н.Н. Токарева s 116
   Oi
P2 Di#11?-3
   i<2=j
   *** is divisible by 4   
   P21 j-4=k
       j-1=l
       j>2=m            *** div 4
      *** RotWord ( L2l / q )
          L2l>24=q
          L2l<8|q=q
      *SubWord( q, L4 / q )
      *** w[j]=w[j-4] ^ SubWord( RotWord( w[j-1] ) ) ^ v[j/4]
      L2k#q#L3m=L2j   
   *** not divisible by 4
   Dj _n
   P22 Dn#3?-23
       j-4=k
       j-1=l
       L2k#L2l=L2j      *** w[j]=w[j-4]^w[j-1]
       Dj ?=22
   P23 ?=2
P3 **
   
   
DefineVi( L1 / L1 )
   01h<24=L1.1
   02h<24=L1.2
   04h<24=L1.3
   08h<24=L1.4
   10h<24=L1.5
   20h<24=L1.6
   40h<24=L1.7
   80h<24=L1.8
   1Bh<24=L1.9
   36h<24=L1.10
   6Ch<24=L1.11
**

************************
*** L1 - SBox
*** q  - 32bit mixing vector
************************
SubWord( q, L2 / q )
    _i
*** geting bytes   
   q&FFh=a
   q>8&FFh=b
   q>16&FFh=c
   q>24&FFh=d
*** SubBytes with sBox: L2a <=> sBox[a]   
   L2d<8|L2c<8|L2b<8|L2a=q
**

