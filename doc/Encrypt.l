************************
*** L1 - in
*** L2 - array of round keys (key schedule)
*** L3 - state
*** L4 - Sbox
************************
Encrypt( L1, L2, L4 / L3 )

   *AddRoundKey( L1 , L2, 0 / L3 )
   Oi
P1 Di#10?-2
   *SubBytes( L3, L4 / L3 )
   *ShiftRows( L3 / L3 )
   *MixColumns( L3 / L3 )
   *AddRoundKey( L3, L2, i / L3 )
   ?=1
P2 *SubBytes( L3, L4 / L3 )
   *ShiftRows( L3 / L3 )
   *AddRoundKey( L3, L2, 10 / L3 )
**
