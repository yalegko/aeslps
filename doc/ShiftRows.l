************************
*** F1 - state
************************
ShiftRows( L1 / L1 )
   _i
P1 Di#Q1?-2
   i;4=t
   *CycleByteShift( L1i, t / a )
   a=L1i
   ?=1
P2 **

************************
*** a - shifting vector
*** n - shift value
************************
CycleByteShift( a, n / a )
   _i
P1 Di#n?-2   
   255<24=q
   a&q>24=t
   a<8|t=a
   ?=1
P2 **
