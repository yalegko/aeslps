************************
*** L1 - state
*** L2 - Sbox
*** m - mask
*** a,b,c,d - bytes of state-string
************************
SubBytes( L1, L2 / L1 )
   _i
P1 Di#Q1?-2
   FFh=m
*** geting bytes   
   L1i&m=a
   L1i>8&m=b
   L1i>16&m=c
   L1i>24&m=d
*** SubBytes with sBox: L2a <=> sBox[a]   
   L2d<8|L2c<8|L2b<8|L2a=L1i
?=1
P2 **
