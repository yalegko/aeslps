AES(/)
   @+F1(128)	      *** key
	@+F2(32000)       *** txt
	@+F3(32000)       *** etxt
	@+F4(32000)       *** dtxt
	@+F7(50)          *** filename
	
	@+L8(256)         *** S-box
	@+L9(256)         *** Inv S-box
   @+L10(4)          *** state
	@+L11(4)          *** initial key
	@+L12(44)         *** key schedule
	@+L13(4)          *** crypted state
	@+L14(4)          *** decrypted state
	   *GenSbox( L8 / L8 )
	   *GenInvSbox( L9 / L9 )
	*** read key
   @'key'>F7
   *fopen(F7,0/n)          *** r-only
   *freadf(n,F1,0,17/k)
   k-1=Q1                  *** bez EOF
   *fclose(n/)
   ***********
      *n2s(Q1,10/F7)
      /' keyLength= '>C /F7>C /'\n'>C 
   *** read text
   OQ7 @'txt'>F7
   *fopen(F7,0/n)          *** r-only
   *freadf(n,F2,0,17/k)
      k-1=Q2               *** bez EOF
   ***********
      *n2s(Q2,10/F7)
      /' TextLength= '>C /F7>C /'\n'>C 
   *********** 
   
   *** Text ( char* ) ---> State ( 32b vectors )
   *BuildState( F2 / L10 )
   /'Text parced\n'>C
   *writemI(L10,4,32/) /'\n'>C
   
   *** Key ( char* ) ---> Key 32b vectors
   *BuildState( F1 / L11 )
   /'Key parced\n'>C
   *writemI(L11,4,32/) 
   
   *** Preparing key
   *KeyExpansion( L11, L8 / L12 )
   /'Key prepared\n'>C
   ****writemI(L12,44,32/) /'\n'>C
   
   *** Encryption
   *Encrypt( L10, L12, L8 / L13 )
   /'\nEncryption done\n'>C
   *writemI(L13,4,32/)
   
   *** eText ( 32b vectors ) ---> eText ( char* )
   *ParseState( L13 / F3 )
   /' eText parced\n'>C
   
   *** writing e_text	
   OQ7 @'etxt'>F7
   *fopen(F7,1/n)          *** w-only
   *fwritef(n,F3,0,Q3/k)
   *********
      *n2s(k,10/F7)
      /' eText length= '>C /F7>C /'\n'>C 
   *********
   
   *** Decryption
   *Decrypt( L13, L12, L9 / L14 )
   /'\nDecryption done\n'>C
   *writemI(L14,4,32/)
   *** dText ( 32b vectors ) ---> dText ( char* )
   *ParseState( L14 / F4 )
   /'dText parced\n '>C
   
   *** writing e_text	
   OQ7 @'dtxt'>F7
   *fopen(F7,1/n)          *** w-only
   *fwritef(n,F4,0,Q4/k)
   *********
      *n2s(k,10/F7)
      /'dText length= '>C /F7>C /'\n'>C 
   *********
   
   
**





