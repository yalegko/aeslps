*** Перевод строки в число ***
s2n(F1,q/a)
   Oi
   P1 ?(i>=Q1)2
      F1i=c
      *c2n(c/b)
      a*q+b=a
      Di ?=1
   P2 
**
*** Перевод цифры в число *** 
c2n(a/b)
   P1 a-'0'=b
      ?(b<10)2
      a-'A'+10=b
      ?(a<'Z')2
      a-'a'+10=b
   P2 
**
*** перевод числа в цифру ***
n2c(a/b) 
	   a+'0'=b ?(a<10)1 a-10+'A'=b 
   P1	** 

*** перевод числа в строку *** 
n2s(a,q/F1) 
	   OQ1 
   P1	a/q=a Z=b *n2c(b/c) c@>F1 a?+1 
   P2	Oi Q1=a-1=j 
   P3	?(i>=j)4 =(F1ij) Di Yj ?=3 
   P4	** 

*** вывод комплекса ***
writemI(L1,n,m/)
      _j
   P0 Dj m=i ?(j>=n)4
   P1 i?-3 Yi 
      L1j&Ii?-2
      /'1 '>C ?=1
   P2 /'. '>C ?=1
   P3 /'\n'>C ?=0
   P4 **
**

********************************************************************
*** Работа с файлами
********************************************************************
*** Открытие файла
*** F1 - имя файла (в текущем каталоге)
*** r - режим открытия: 0 - чтение, 1 - запись, 2 - чтение/запись
*** n  - дескриптор файла (<0 при отказе)
********************************************************************
fopen(F1,r/n)
   0@>F1
   {mov eax,5}
   {mov ebx,[ebp+220]}
   {mov ecx,[ebp+72]}
   {int 80h}
   {mov [ebp+56],eax}
   **
********************************************************************
*** Закрытие файла
*** n  - дескриптор файла
********************************************************************
fclose(n/)
   {mov eax,6}
   {mov ebx,[ebp+56]}
   {int 80h}
   **
********************************************************************
*** Чтение из файла в символьный комплекс
*** n  - дескриптор файла
*** F1 - файл
*** b - адрес в F1 первого считанного байта
*** l - число запрашиваемых байт
*** k - число считанных байтов (код ошибки для <0)
********************************************************************
freadf(n,F1,b,l/k)
   {mov eax,3}
   {mov ebx,[ebp+56]}
   {mov ecx,[ebp+220]}
   {add ecx,[ebp+8]}
   {mov edx,[ebp+48]}
   {int 80h}
   {mov [ebp+44],eax}
   **
********************************************************************
*** Запись из символьного комплекса в файл
*** n  - дескриптор файла
*** F1 - файл
*** b - адрес в F1 первого записываемого байта
*** l - число записывемых байт
*** k - число записанных байтов (код ошибки для <0)
********************************************************************
fwritef(n,F1,b,l/k)
   {mov eax,4}
   {mov ebx,[ebp+56]}
   {mov ecx,[ebp+220]}
   {add ecx,[ebp+8]}
   {mov edx,[ebp+48]}
   {int 80h}
   {mov [ebp+44],eax}
   **



********************************************************************
*** AES FUNCTIONS
********************************************************************

************************
*** Convert 16 bytes into 4x32-bit vectors
*** F1 - char[16]
*** L2 - state
************************
BuildState( F1 / L2 )
   4=Q2
   _i
P3 Di#Q2?-4
   0=L2i
   ?=3
P4 _i
P1 Di#Q2?-2
   _j
   P11 Dj#Q2?-1
       i<2+j=k
       F1k | L2j = L2j
       i#3?-12
         L2j < 8 = L2j
   P12 ?=11
P2 **

************************
*** Convert state(4x32-bit vectors) into char*
*** F2 - char[16]
*** L1 - state
************************
ParseState( L1 / F2 )
@+L3(4)   
   16=Q2
    4=Q3
   _i
P3 Di#4?-4
   L1i=L3i
   ?=3
P4 _i      
P1 Di#4?-2
      _j 
      i<2=k 15-k=k
P11   Dj#4?-1
         k-j=m
         3-j=l
         L3l & FFh = F2m
            L3l > 8 = L3l      
P12   ?=11
P2 **     

addRandomSymbols( F1, k / F1 )
   T;1000=n
   _i
P0 Di#n?-1
   X  
   ?=0
   
P1 k#16?-2
   X=F1k
   Dk
   ?=1
P2 **


************************
*** L1 - in
*** L2 - array of round keys (key schedule)
*** L3 - state
*** L4 - Sbox
************************
Encrypt( L1, L2, L4 / L3 )

   *AddRoundKey( L1 , L2, 0 / L3 )
   Oi
P1 Di#10?-2
   *SubBytes( L3, L4 / L3 )
   *ShiftRows( L3 / L3 )
   *MixColumns( L3 / L3 )
   *AddRoundKey( L3, L2, i / L3 )
   ?=1
P2 *SubBytes( L3, L4 / L3 )
   *ShiftRows( L3 / L3 )
   *AddRoundKey( L3, L2, 10 / L3 )
**

************************
*** L1 - state
*** L2 - array of round keys
*** n  - round number
*** L3 - new round key
************************
AddRoundKey( L1 , L2, n / L3 )
   Q1=Q3
   n<2=n    *** round*4 -  index of the first key's part
      L1.0#L2n=L3.0 
   Dn L1.1#L2n=L3.1
   Dn L1.2#L2n=L3.2
   Dn L1.3#L2n=L3.3
P2 **

************************
*** L1 - key
*** L2 - array of round keys
*** L3 - Vi-constants ( s116 )
*** L4 - SBox
************************
KeyExpansion( L1, L4 / L2 )
@+L3(12)
*DefineVi( L3 / L3 )

   44=Q2
   _i
*** 1-4 without changes
P1 Di#4?-12
   L1i=L2i
   ?=1
P12
*** extension; Симметричная криптография  Н.Н. Токарева s 116
   Oi
P2 Di#11?-3
   i<2=j
   *** is divisible by 4   
   P21 j-4=k
       j-1=l
       j>2=m            *** div 4
      *** RotWord ( L2l / q )
          L2l>24=q
          L2l<8|q=q
      *SubWord( q, L4 / q )
      *** w[j]=w[j-4] ^ SubWord( RotWord( w[j-1] ) ) ^ v[j/4]
      L2k#q#L3m=L2j   
   *** not divisible by 4
   Dj _n
   P22 Dn#3?-23
       j-4=k
       j-1=l
       L2k#L2l=L2j      *** w[j]=w[j-4]^w[j-1]
       Dj ?=22
   P23 ?=2
P3 **
   
   
DefineVi( L1 / L1 )
   01h<24=L1.1
   02h<24=L1.2
   04h<24=L1.3
   08h<24=L1.4
   10h<24=L1.5
   20h<24=L1.6
   40h<24=L1.7
   80h<24=L1.8
   1Bh<24=L1.9
   36h<24=L1.10
   6Ch<24=L1.11
**

************************
*** L1 - SBox
*** q  - 32bit mixing vector
************************
SubWord( q, L2 / q )
    _i
*** geting bytes   
   q&FFh=a
   q>8&FFh=b
   q>16&FFh=c
   q>24&FFh=d
*** SubBytes with sBox: L2a <=> sBox[a]   
   L2d<8|L2c<8|L2b<8|L2a=q
**

************************
*** L1 - state
*** i - examining string
*** j - number of byte-column
************************
MixColumns( L1 / L1 )
@+L2(4)
   4=Q2
   _j
   P11 Dj#4?-2
       j<3=q                         *** j*8 ; q - first bit of examining column
       _k
       P111 Dk#4?-112
            L1k>q&FFh =L2k      *** get byte-column    
            ?=111
       P112 *MixColumn( L2 / L2 )
            _k
       P113 Dk#4?-114
            FFh<q~ & L1k =L1k        *** clear byte-column
            L2k<q | L1k =L1k         *** return byte-column    
            ?=113
       P114     
            ?=11
P2 **      


************************
*** L2 - byte-column == col[4] ( L2.0 == a[0][i], L2.1 == a[1][i] ... ) 
*** L3 - temp xtime array == xt[4]
*** L4 - temp column array == tmp[4]
************************
MixColumn( L2 / L2 )
@+L3(4)
@+L4(4)
   Q2=Q3=Q4 _i
P1 Di#Q3?-2
   *xtime( L2i / a )
   a=L3i
   ?=1
*** diffusing ( CD.pdf p175 )   
P2 L3.0 # L3.1 # L2.1 # L2.2 # L2.3 = L4.0   
   L2.0 # L3.1 # L3.2 # L2.2 # L2.3 = L4.1
   L2.0 # L2.1 # L3.2 # L3.3 # L2.3 = L4.2
   L3.0 # L2.0 # L2.1 # L2.2 # L3.3 = L4.3
   _i
*** rewriting tmp => col
P3 Di#4?-4
   L4i=L2i
   ?=3
P4 **

************************
*** mul by x in GF[2][x]/v(x)
************************
xtime( x / x )
   x<1=x
   *** reduce to GF
   x&100h?-0 x#11Bh=x
P0 **

************************
*** L1 - state
************************
ShiftRows( L1 / L1 )
   _i
P1 Di#Q1?-2
   i&3<3=n
   32-n=t
   L1i>t=q
   L1i<n|q=L1i
   ?=1
P2 **

************************
*** L1 - state
*** L2 - Sbox
*** m - mask
*** a,b,c,d - bytes of state-string
************************
SubBytes( L1, L2 / L1 )
   _i
P1 Di#Q1?-2
   FFh=m
*** geting bytes   
   L1i&m=a
   L1i>8&m=b
   L1i>16&m=c
   L1i>24&m=d
*** SubBytes with sBox: L2a <=> sBox[a]   
   L2d<8|L2c<8|L2b<8|L2a=L1i
?=1
P2 **

************************
*** L1 - in
*** L2 - array of round keys
*** L3 - state
*** L4 - invSbox
************************
Decrypt( L1, L2, L4 / L3 )
   *AddRoundKey( L1 , L2, 10 / L3 )
   10=i
P1 Yi?-2
   *InvShiftRows( L3 / L3 )
   *SubBytes( L3, L4 / L3 )
   *AddRoundKey( L3, L2, i / L3 )
   *InvMixColumns( L3 / L3 )
   ?=1
P2 *InvShiftRows( L3 / L3 )
   *SubBytes( L3, L4 / L3 )
   *AddRoundKey( L3, L2, 0 / L3 )
   
**

************************
*** L1 - state
************************
InvShiftRows( L1 / L1 )
   _i
P1 Di#Q1?-2
   i&3<3=n
   32-n=t
   L1i<t=q
   L1i>n|q=L1i
   ?=1
P2 **

************************
*** L1 - state
*** i - examining string
*** j - number of byte-column
************************
InvMixColumns( L1 / L1 )
@+L2(4)
   4=Q2
   _j
   P11 Dj#4?-2
       j<3=q                         *** j div 8 ; q - first bit of examining column
       _k
       P111 Dk#4?-112
            L1k>q&FFh =L2k           *** get byte-column    
            ?=111
       P112 *InvMixColumn( L2 / L2 ) 
            _k
       P113 Dk#4?-114
            FFh<q~ & L1k =L1k        *** clear byte-column
            L2k<q | L1k =L1k         *** return byte-column    
            ?=113
       P114     
            ?=11
P2 **      


************************
*** L2 - byte-column == col[4] ( L2.0 == a[0][i], L2.1 == a[1][i] ... ) 
*** L3 - temp gf_mul array
*** L4 - temp column array == tmp[4]
************************
InvMixColumn( L2 / L2 )
@+L3(4)
@+L4(4)
   4=Q3=Q4 _i
P1 Di#4?-2
   i=q
      *gf_mul( L2q, 14 / a )
      a=L3.0
   q+1&3=q
      *gf_mul( L2q, 11 / a )
      a=L3.1
   q+1&3=q
      *gf_mul( L2q, 13 / a )
      a=L3.2
   q+1&3=q
      *gf_mul( L2q, 9 / a )
      a=L3.3
   L3.0 # L3.1 # L3.2 # L3.3 = L4i
?=1   

P2   _i
*** rewriting tmp => col
P3 Di#4?-4
   L4i=L2i
   ?=3
P4 **


************************
*** multiplicate a by b in GF[2][x]/v(x)
************************
gf_mul( a, b / c )
   Oc
P1 a?-2
      *** if bit of a is set add b
      a&1?-11 c#b=c
      *** multiply b by x
P11   b<1=b
      *** reduce mod 0x11B == v(x)
      b&100h?-12 b#11Bh=b
P12   *** get next bit of a
      a>1=a
   ?=1
P2 **

************************
*** Generate sBox for fast subbytes shuffle
*** L1 - sBox
************************
GenSbox( L1 / L1 )
   63h@>L1
   7Ch@>L1
   77h@>L1
   7Bh@>L1
   F2h@>L1
   6Bh@>L1
   6Fh@>L1
   C5h@>L1
   30h@>L1
   01h@>L1
   67h@>L1
   2Bh@>L1
   FEh@>L1
   D7h@>L1
   ABh@>L1
   76h@>L1
   CAh@>L1
   82h@>L1
   C9h@>L1
   7Dh@>L1
   FAh@>L1
   59h@>L1
   47h@>L1
   F0h@>L1
   ADh@>L1
   D4h@>L1
   A2h@>L1
   AFh@>L1
   9Ch@>L1
   A4h@>L1
   72h@>L1
   C0h@>L1
   B7h@>L1
   FDh@>L1
   93h@>L1
   26h@>L1
   36h@>L1
   3Fh@>L1
   F7h@>L1
   CCh@>L1
   34h@>L1
   A5h@>L1
   E5h@>L1
   F1h@>L1
   71h@>L1
   D8h@>L1
   31h@>L1
   15h@>L1
   04h@>L1
   C7h@>L1
   23h@>L1
   C3h@>L1
   18h@>L1
   96h@>L1
   05h@>L1
   9Ah@>L1
   07h@>L1
   12h@>L1
   80h@>L1
   E2h@>L1
   EBh@>L1
   27h@>L1
   B2h@>L1
   75h@>L1
   09h@>L1
   83h@>L1
   2Ch@>L1
   1Ah@>L1
   1Bh@>L1
   6Eh@>L1
   5Ah@>L1
   A0h@>L1
   52h@>L1
   3Bh@>L1
   D6h@>L1
   B3h@>L1
   29h@>L1
   E3h@>L1
   2Fh@>L1
   84h@>L1
   53h@>L1
   D1h@>L1
   00h@>L1
   EDh@>L1
   20h@>L1
   FCh@>L1
   B1h@>L1
   5Bh@>L1
   6Ah@>L1
   CBh@>L1
   BEh@>L1
   39h@>L1
   4Ah@>L1
   4Ch@>L1
   58h@>L1
   CFh@>L1
   D0h@>L1
   EFh@>L1
   AAh@>L1
   FBh@>L1
   43h@>L1
   4Dh@>L1
   33h@>L1
   85h@>L1
   45h@>L1
   F9h@>L1
   02h@>L1
   7Fh@>L1
   50h@>L1
   3Ch@>L1
   9Fh@>L1
   A8h@>L1
   51h@>L1
   A3h@>L1
   40h@>L1
   8Fh@>L1
   92h@>L1
   9Dh@>L1
   38h@>L1
   F5h@>L1
   BCh@>L1
   B6h@>L1
   DAh@>L1
   21h@>L1
   10h@>L1
   ffh@>L1
   F3h@>L1
   D2h@>L1
   CDh@>L1
   0Ch@>L1
   13h@>L1
   ECh@>L1
   5Fh@>L1
   97h@>L1
   44h@>L1
   17h@>L1
   C4h@>L1
   A7h@>L1
   7Eh@>L1
   3Dh@>L1
   64h@>L1
   5Dh@>L1
   19h@>L1
   73h@>L1
   60h@>L1
   81h@>L1
   4Fh@>L1
   DCh@>L1
   22h@>L1
   2Ah@>L1
   90h@>L1
   88h@>L1
   46h@>L1
   EEh@>L1
   B8h@>L1
   14h@>L1
   DEh@>L1
   5Eh@>L1
   0Bh@>L1
   DBh@>L1
   E0h@>L1
   32h@>L1
   3Ah@>L1
   0Ah@>L1
   49h@>L1
   06h@>L1
   24h@>L1
   5Ch@>L1
   C2h@>L1
   D3h@>L1
   ACh@>L1
   62h@>L1
   91h@>L1
   95h@>L1
   E4h@>L1
   79h@>L1
   E7h@>L1
   C8h@>L1
   37h@>L1
   6Dh@>L1
   8Dh@>L1
   D5h@>L1
   4Eh@>L1
   A9h@>L1
   6Ch@>L1
   56h@>L1
   F4h@>L1
   EAh@>L1
   65h@>L1
   7Ah@>L1
   AEh@>L1
   08h@>L1
   BAh@>L1
   78h@>L1
   25h@>L1
   2Eh@>L1
   1Ch@>L1
   A6h@>L1
   B4h@>L1
   C6h@>L1 
   E8h@>L1 
   DDh@>L1 
   74h@>L1 
   1Fh@>L1 
   4Bh@>L1 
   BDh@>L1 
   8Bh@>L1 
   8Ah@>L1
   70h@>L1 
   3Eh@>L1 
   B5h@>L1 
   66h@>L1 
   48h@>L1 
   03h@>L1 
   F6h@>L1 
   0Eh@>L1 
   61h@>L1 
   35h@>L1 
   57h@>L1 
   B9h@>L1 
   86h@>L1 
   C1h@>L1 
   1Dh@>L1 
   9Eh@>L1
   E1h@>L1 
   F8h@>L1 
   98h@>L1 
   11h@>L1 
   69h@>L1 
   D9h@>L1 
   8Eh@>L1 
   94h@>L1 
   9Bh@>L1 
   1Eh@>L1 
   87h@>L1 
   E9h@>L1 
   CEh@>L1 
   55h@>L1 
   28h@>L1 
   DFh@>L1
   8Ch@>L1
   A1h@>L1 
   89h@>L1 
   0Dh@>L1 
   BFh@>L1 
   E6h@>L1 
   42h@>L1 
   68h@>L1 
   41h@>L1 
   99h@>L1 
   2Dh@>L1 
   0Fh@>L1 
   B0h@>L1 
   54h@>L1 
   BBh@>L1 
   16h@>L1
**
************************
*** L1 - inverted sBox for decrypting
************************
GenInvSbox( L1 / L1 )

52h@>L1
09h@>L1
6ah@>L1
d5h@>L1
30h@>L1
36h@>L1
a5h@>L1
38h@>L1
bfh@>L1
40h@>L1
a3h@>L1
9eh@>L1
81h@>L1
f3h@>L1
d7h@>L1
fbh@>L1
7ch@>L1
e3h@>L1
39h@>L1
82h@>L1
9bh@>L1
2fh@>L1
ffh@>L1
87h@>L1
34h@>L1
8eh@>L1
43h@>L1
44h@>L1
c4h@>L1
deh@>L1
e9h@>L1
cbh@>L1
54h@>L1
7bh@>L1
94h@>L1
32h@>L1
a6h@>L1
c2h@>L1
23h@>L1
3dh@>L1
eeh@>L1
4ch@>L1
95h@>L1
0bh@>L1
42h@>L1
fah@>L1
c3h@>L1
4eh@>L1
08h@>L1
2eh@>L1
a1h@>L1
66h@>L1
28h@>L1
d9h@>L1
24h@>L1
b2h@>L1
76h@>L1
5bh@>L1
a2h@>L1
49h@>L1
6dh@>L1
8bh@>L1
d1h@>L1
25h@>L1
72h@>L1
f8h@>L1
f6h@>L1
64h@>L1
86h@>L1
68h@>L1
98h@>L1
16h@>L1
d4h@>L1
a4h@>L1
5ch@>L1
cch@>L1
5dh@>L1
65h@>L1
b6h@>L1
92h@>L1
6ch@>L1
70h@>L1
48h@>L1
50h@>L1
fdh@>L1
edh@>L1
b9h@>L1
dah@>L1
5eh@>L1
15h@>L1
46h@>L1
57h@>L1
a7h@>L1
8dh@>L1
9dh@>L1
84h@>L1
90h@>L1
d8h@>L1
abh@>L1
00h@>L1
8ch@>L1
bch@>L1
d3h@>L1
0ah@>L1
f7h@>L1
e4h@>L1
58h@>L1
05h@>L1
b8h@>L1
b3h@>L1
45h@>L1
06h@>L1
d0h@>L1
2ch@>L1
1eh@>L1
8fh@>L1
cah@>L1
3fh@>L1
0fh@>L1
02h@>L1
c1h@>L1
afh@>L1
bdh@>L1
03h@>L1
01h@>L1
13h@>L1
8ah@>L1
6bh@>L1
3ah@>L1
91h@>L1
11h@>L1
41h@>L1
4fh@>L1
67h@>L1
dch@>L1
eah@>L1
97h@>L1
f2h@>L1
cfh@>L1
ceh@>L1
f0h@>L1
b4h@>L1
e6h@>L1
73h@>L1
96h@>L1
ach@>L1
74h@>L1
22h@>L1
e7h@>L1
adh@>L1
35h@>L1
85h@>L1
e2h@>L1
f9h@>L1
37h@>L1
e8h@>L1
1ch@>L1
75h@>L1
dfh@>L1
6eh@>L1
47h@>L1
f1h@>L1
1ah@>L1
71h@>L1
1dh@>L1
29h@>L1
c5h@>L1
89h@>L1
6fh@>L1
b7h@>L1
62h@>L1
0eh@>L1
aah@>L1
18h@>L1
beh@>L1
1bh@>L1
fch@>L1
56h@>L1
3eh@>L1
4bh@>L1
c6h@>L1
d2h@>L1
79h@>L1
20h@>L1
9ah@>L1
dbh@>L1
c0h@>L1
feh@>L1
78h@>L1
cdh@>L1
5ah@>L1
f4h@>L1
1fh@>L1
ddh@>L1
a8h@>L1
33h@>L1
88h@>L1
07h@>L1
c7h@>L1
31h@>L1
b1h@>L1
12h@>L1
10h@>L1
59h@>L1
27h@>L1
80h@>L1
ech@>L1
5fh@>L1
60h@>L1
51h@>L1
7fh@>L1
a9h@>L1
19h@>L1
b5h@>L1
4ah@>L1
0dh@>L1
2dh@>L1
e5h@>L1
7ah@>L1
9fh@>L1
93h@>L1
c9h@>L1
9ch@>L1
efh@>L1
a0h@>L1
e0h@>L1
3bh@>L1
4dh@>L1
aeh@>L1
2ah@>L1
f5h@>L1
b0h@>L1
c8h@>L1
ebh@>L1
bbh@>L1
3ch@>L1
83h@>L1
53h@>L1
99h@>L1
61h@>L1
17h@>L1
2bh@>L1
04h@>L1
7eh@>L1
bah@>L1
77h@>L1
d6h@>L1
26h@>L1
e1h@>L1
69h@>L1
14h@>L1
63h@>L1
55h@>L1
21h@>L1
0ch@>L1
7dh@>L1

**

   
   
   
   
   
   
   
   
