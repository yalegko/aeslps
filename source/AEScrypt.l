AES(/)
   @+F1(17)	         *** key
	@+F2(17)          *** txt
	@+F3(17)          *** etxt
	@+F7(50)          *** filename
	
	@+L10(4)          *** state
	@+L11(4)          *** initial key
	@+L12(44)         *** key schedule
	@+L13(4)          *** crypted state
	@+L4(256)         *** S-box
      *GenSbox( L4 / L4 )
	
	*** read key
   @'key'>F7
   *fopen(F7,0/n)          *** r-only
   *freadf(n,F1,0,17/k)
   k-1=Q1                  *** bez EOF
   *fclose(n/)
   ***********
  
   *** Key ( char* ) ---> Key 32b vectors
   *BuildState( F1 / L11 )
   *** Preparing key
   *KeyExpansion( L11, L4 / L12 )
   
   *** open texts
   OQ7 @'txt'>F7
   *fopen(F7,0/t)          *** r-only
   OQ7 @'etxt'>F7
   *fopen(F7,1/e)          *** w-only
   
   
P1 *freadf(t,F2,0,16/k)
   ?(k<16)2
      16=Q2
   *** Text ( char* ) ---> State ( 32b vectors )
   *BuildState( F2 / L10 )
   
   *** Encryption
   *Encrypt( L10, L12, L4 / L13 )
   *** eText ( 32b vectors ) ---> eText ( char* )
   *ParseState( L13 / F3 )

   *** writing e_text	
   *fwritef(e,F3,0,16/l)

   ?=1
   *** write random symbols to encrypt half-block 
P2  
   *addRandomSymbols( F2, k / F2 )
   
   *BuildState( F2 / L10 )
   
   *** Encryption
   *Encrypt( L10, L12, L4 / L13 )
   *** eText ( 32b vectors ) ---> eText ( char* )
   *ParseState( L13 / F3 )

   *** writing e_text	
   *fwritef(e,F3,0,16/l)

   
    
P3 *fclose(t/)
   *fclose(e/)

**   

   
   
   
   






